<?php
/**
 * SettingTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\BasketsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi Baskets
 *
 * Ensi Baskets
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the model.
 */

namespace Ensi\BasketsClient;

use PHPUnit\Framework\TestCase;

/**
 * SettingTest Class Doc Comment
 *
 * @category    Class
 * @description Setting
 * @package     Ensi\BasketsClient
 * @author      OpenAPI Generator team
 * @link        https://openapi-generator.tech
 */
class SettingTest extends TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Setting"
     */
    public function testSetting()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "code"
     */
    public function testPropertyCode()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "updated_at"
     */
    public function testPropertyUpdatedAt()
    {
    }

    /**
     * Test attribute "value"
     */
    public function testPropertyValue()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }
}
