# # BasketItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id элемента корзины | [optional] 
**basket_id** | **int** | ID корзины | [optional] 
**offer_id** | **int** | ID оффера | [optional] 
**seller_id** | **int** | ID продавца | [optional] 
**product_id** | **int** | ID товара | [optional] 
**qty** | **float** | Кол-во товара | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | дата обновления | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


