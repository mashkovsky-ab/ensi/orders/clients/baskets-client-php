# # PatchBasketSellerResponseData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**was_change** | **bool** | Был ли изменен состав корзины | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


