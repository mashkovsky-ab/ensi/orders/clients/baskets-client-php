# Ensi\BasketsClient\CustomerBasketsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteBasketCustomer**](CustomerBasketsApi.md#deleteBasketCustomer) | **DELETE** /baskets/baskets/customer:delete | Удалить текущую корзину пользователя
[**patchBasketCustomerSeller**](CustomerBasketsApi.md#patchBasketCustomerSeller) | **PATCH** /baskets/baskets/customer:patch-seller | Установить продавца в корзину пользователя
[**searchBasketCustomer**](CustomerBasketsApi.md#searchBasketCustomer) | **POST** /baskets/baskets/customer:search-one | Получить информацию о корзине пользователя
[**setBasketCustomerItem**](CustomerBasketsApi.md#setBasketCustomerItem) | **POST** /baskets/baskets/customer:set-item | Установить товар в корзину пользователя



## deleteBasketCustomer

> \Ensi\BasketsClient\Dto\EmptyDataResponse deleteBasketCustomer($delete_basket_customer_request)

Удалить текущую корзину пользователя

Удалить текущую корзину пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CustomerBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$delete_basket_customer_request = new \Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest(); // \Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest | 

try {
    $result = $apiInstance->deleteBasketCustomer($delete_basket_customer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerBasketsApi->deleteBasketCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_basket_customer_request** | [**\Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest**](../Model/DeleteBasketCustomerRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchBasketCustomerSeller

> \Ensi\BasketsClient\Dto\PatchBasketSellerResponse patchBasketCustomerSeller($patch_basket_seller_request)

Установить продавца в корзину пользователя

Установить продавца в корзину пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CustomerBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$patch_basket_seller_request = new \Ensi\BasketsClient\Dto\PatchBasketSellerRequest(); // \Ensi\BasketsClient\Dto\PatchBasketSellerRequest | 

try {
    $result = $apiInstance->patchBasketCustomerSeller($patch_basket_seller_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerBasketsApi->patchBasketCustomerSeller: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_basket_seller_request** | [**\Ensi\BasketsClient\Dto\PatchBasketSellerRequest**](../Model/PatchBasketSellerRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\PatchBasketSellerResponse**](../Model/PatchBasketSellerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchBasketCustomer

> \Ensi\BasketsClient\Dto\SearchBasketCustomerResponse searchBasketCustomer($search_basket_customer_request)

Получить информацию о корзине пользователя

Получить информацию о корзине пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CustomerBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_basket_customer_request = new \Ensi\BasketsClient\Dto\SearchBasketCustomerRequest(); // \Ensi\BasketsClient\Dto\SearchBasketCustomerRequest | 

try {
    $result = $apiInstance->searchBasketCustomer($search_basket_customer_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerBasketsApi->searchBasketCustomer: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_basket_customer_request** | [**\Ensi\BasketsClient\Dto\SearchBasketCustomerRequest**](../Model/SearchBasketCustomerRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\SearchBasketCustomerResponse**](../Model/SearchBasketCustomerResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## setBasketCustomerItem

> \Ensi\BasketsClient\Dto\EmptyDataResponse setBasketCustomerItem($set_basket_item_request)

Установить товар в корзину пользователя

Установить товар в корзину пользователя

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\BasketsClient\Api\CustomerBasketsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$set_basket_item_request = new \Ensi\BasketsClient\Dto\SetBasketItemRequest(); // \Ensi\BasketsClient\Dto\SetBasketItemRequest | 

try {
    $result = $apiInstance->setBasketCustomerItem($set_basket_item_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CustomerBasketsApi->setBasketCustomerItem: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **set_basket_item_request** | [**\Ensi\BasketsClient\Dto\SetBasketItemRequest**](../Model/SetBasketItemRequest.md)|  |

### Return type

[**\Ensi\BasketsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

