<?php

namespace Ensi\BasketsClient;

class BasketsClientProvider
{
    /** @var string[] */
    public static $apis = ['\Ensi\BasketsClient\Api\CommonApi', '\Ensi\BasketsClient\Api\CustomerBasketsApi'];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\BasketsClient\Dto\PatchBasketSellerResponse',
        '\Ensi\BasketsClient\Dto\Setting',
        '\Ensi\BasketsClient\Dto\PaginationTypeEnum',
        '\Ensi\BasketsClient\Dto\PatchBasketSellerRequest',
        '\Ensi\BasketsClient\Dto\PatchSeveralSettingsRequest',
        '\Ensi\BasketsClient\Dto\ErrorResponse2',
        '\Ensi\BasketsClient\Dto\IncludeId',
        '\Ensi\BasketsClient\Dto\Basket',
        '\Ensi\BasketsClient\Dto\BasketIncludes',
        '\Ensi\BasketsClient\Dto\ErrorResponse',
        '\Ensi\BasketsClient\Dto\SettingFillableProperties',
        '\Ensi\BasketsClient\Dto\SettingsResponse',
        '\Ensi\BasketsClient\Dto\PatchBasketSellerResponseData',
        '\Ensi\BasketsClient\Dto\SettingReadOnlyProperties',
        '\Ensi\BasketsClient\Dto\BasketReadonlyProperties',
        '\Ensi\BasketsClient\Dto\ModelInterface',
        '\Ensi\BasketsClient\Dto\SetBasketItemRequestItems',
        '\Ensi\BasketsClient\Dto\EmptyDataResponse',
        '\Ensi\BasketsClient\Dto\SettingsForPatch',
        '\Ensi\BasketsClient\Dto\SettingCodeEnum',
        '\Ensi\BasketsClient\Dto\DeleteBasketCustomerRequest',
        '\Ensi\BasketsClient\Dto\SearchBasketCustomerRequest',
        '\Ensi\BasketsClient\Dto\SearchBasketCustomerResponse',
        '\Ensi\BasketsClient\Dto\Error',
        '\Ensi\BasketsClient\Dto\SetBasketItemRequest',
        '\Ensi\BasketsClient\Dto\BasketItem',
    ];

    /** @var string */
    public static $configuration = '\Ensi\BasketsClient\Configuration';
}
